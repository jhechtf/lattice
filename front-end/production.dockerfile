FROM node:11 as build
RUN mkdir -p /var/app
WORKDIR /var/app
ADD . .
RUN yarn && yarn build

FROM nginx as production
COPY --from=build /var/app/dist /usr/share/nginx/html
