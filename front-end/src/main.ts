import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import Loading from '@components/Loading.vue';
// Grab the plugin
import {plugin as LtdbPlugin} from '@/plugins/ltdb';
import BootstrapVue from 'bootstrap-vue';
// Global element, just register it here.
Vue.component('Loading', Loading);
Vue.config.productionTip = false;
// use the LTDB Plugin
Vue.use(LtdbPlugin, {
  apiKey: 'bcb2c525d6757129e5abc85ac4a5b2e9'
});
Vue.use(BootstrapVue);
new Vue({
  router,
  store,
  render: (h) => h(App)
}).$mount('#app');
