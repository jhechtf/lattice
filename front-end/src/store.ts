import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    searchQuery: '',
    favoriteMovies: []
  },
  mutations: {
    updateSearch(state, data) {
      state.searchQuery = data.searchQuery;
    },
    addFavoriteMovie(state, movieId) {
      state.favoriteMovies.push(movieId);
    },
    removeFavoriteMovie(state, movieId) {
      const movieIndex = state.favoriteMovies.indexOf(movieId);
      if( movieIndex !== -1) {
        state.favoriteMovies.splice(movieIndex, 1);
      }
    }
  },
  actions: {

  }
});
