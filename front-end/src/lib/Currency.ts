// tslint:disable: variable-name
export const CurrencyFormat = (n: number, fixedLength: number = 0, delimeter: string = ',', decimalDelimeter: string= '.' ): string => {
    const a: string[] = [];
    let s = n.toFixed(fixedLength);
    let decimal: string | boolean = false;

    // If we have a dot, it means we have a decimal part.
    if(s.indexOf('.') !== -1) {
       decimal = s.slice(-s.indexOf('.') );
       s = s.slice(0, -s.indexOf('.') );
    }
    while(s.length) {
        a.unshift(s.slice(-3));
        s = s.slice(0,-3);
    }

    return a.join(delimeter) + (decimal ? `${decimalDelimeter}${decimal}` : '');
};
