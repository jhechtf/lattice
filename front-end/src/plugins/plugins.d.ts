// Import the LTDB class
import {Ltdb} from '@/plugins/ltdb';
import {Route} from 'vue-router';
// Declare the Vue module
declare module 'vue/types/vue' {
  // export our Vue interface with our new property on it.
  export interface Vue {
    $ltdb: Ltdb;
    $route: Route;
  }
}
