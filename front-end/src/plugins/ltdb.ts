import axios, { AxiosInstance, AxiosRequestConfig } from 'axios';
import {VueConstructor} from 'vue';

export interface LtdbPluginOptions {
    apiKey: string;
}

export class Ltdb {
    // API Key
    protected _apiKey: string;
    // Axios instance
    protected $http: AxiosInstance;
    // TMDB base image URL
    protected $base: string = '//image.tmdb.org/t/p';
    // Constructor
    constructor(apiKey: string, options?: object) {
        // Set the API key
        this._apiKey = apiKey;
        // create the instance
        this.$http = axios.create({
            baseURL: 'http://localhost:3000/api/'
        });
    }
    /**
     * @param url the url relative to the base URL that we will be calling.
     * @returns a promise that will either resolve with data or throw an error
     */
    public get(url: string, options?: AxiosRequestConfig): Promise<any> {
        return this.$http.get(url, options || {});
    }
    /**
     * @param image the image name
     * @param size the image size
     * @description builds an image Uri
     * @returns a string that represents the URI to the given image+size combo
     */
    public buildImageUri(image: string= '', size: string= 'original'): string {
        if(image === '' || image == null) return '';
        return `${this.$base}/${size}/${image}`;
    }
}
// Disabling this for the linter because otherwise it gets confusing.
// tslint:disable:variable-name
export const plugin = {
    install(Vue: VueConstructor, pluginOptions: LtdbPluginOptions) {
        Vue.prototype.$ltdb = new Ltdb(pluginOptions.apiKey);
    }
};
