# Lattice Test Program

## Installing Docker

If you do not have docker, you can get it via the [Getting Starte](https://www.docker.com/get-started) page on the Docker website

## Base Step

In order to make sure that the node_modules are installed, you should run the following two commands from the project root directory (assuming you have docker installed already):

```
docker-compose -f docker-compose.builder.yml --rm be-install
docker-compose -f docker-compsoe.builder.yml --rm fe-install
```

## Dev Mode

I use Docker + Docker Compose for most things, so for this you will only need Docker + Docker Compose.

1. I recommend running `docker-compose build` in the main directory. This can alert you to any problems that you may encounter bringing the project up later.
2. Once the images have been built, you can run `docker-compose up` or `docker-compose up -d` to start it in Daemon mode (i.e. you won't see the output from the containers)

## "Production" Mode

Production mode uses the Docker as well, but it utilizes the `production.dockerfile` instead of the default `Dockerfile`, which is intended for development purposes. 

To get production mode up I recommend doing the following:

1. `docker-compose -f docker-compose-production.yml build`
2. `docker-compose -f docker-compose-production.yml up -d`

This should start the front-end server on port `80` by default, so if you have a server running on that port already then you should change it to a port that is unused.