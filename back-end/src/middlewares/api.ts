import { Context } from 'koa';
import {config} from 'dotenv';
import { TmdbWrapper } from '../classes/tmdb';
config();
export const ensureJson = async (ctx: Context, next) => {
    ctx.type = 'json';
    // ctx.body = [];
    return next();
};

export const addTmdbToCtx = async (ctx: Context, next) => {
    ctx.$tmdb = new TmdbWrapper(process.env.API_KEY);
    return next();
};
