// import {Context} from 'koa';
import {TmdbWrapper} from '../classes/tmdb';

// We have to declare these two modules otherwise you get type agreement errors and that's not good.
declare module 'koa' {
    export interface Context {
        $tmdb: TmdbWrapper;
    }
}

declare module 'koa-router' {
    export interface IRouterParamContext {
        $tmdb: TmdbWrapper;
    }
}