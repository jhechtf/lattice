import * as Koa from 'koa';
import {routes} from './routes';
import {config} from 'dotenv';
import * as Cors from '@koa/cors';
import * as Middlewares from './middlewares';
const app = new Koa();
// We needed to add in cors, otherwise the requests from :8080 were being rejected from :3000
app.use(Cors());
// add in our .env variables
config();
// if we're in development, let's go ahead and just give ourselves a quick log.
if(process.env.NODE_ENV==='development') {
    console.info('Since we are in development mode, some basic information about any requests will be output to the console.');
    app.use((ctx: Koa.Context, next)=> {
        console.log(ctx.method, ctx.url, ctx.originalUrl);
        return next();
    });
}

// add in the middlewares
for(const middleware in Middlewares) {
    if(Middlewares.hasOwnProperty(middleware)) {
        app.use(Middlewares[middleware]);
    }
}

// use all of the routes we've created
app.use(routes.routes());

// Listen on port 3000
const server = app.listen(process.env.LISTEN_PORT || 3000);

export {
    server
};
