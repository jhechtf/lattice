import * as Router from 'koa-router';
import { Context } from 'koa';

const router = new Router({
    prefix: '/search'
});

// get all movies
router.get('/', async (ctx: Context, next)=> {
    // If we don't have a query parameter, move on.
    if(!ctx.query.query) return ctx.body = [], next();
    const {data} = await ctx.$tmdb.get('search/movie', {
        params: {
            query: ctx.query.query,
            page: ctx.query.page || 1
        }
    });
    return next().then(()=> {
        ctx.body = data;
    });
});

export {
    router
};
