import * as Router from 'koa-router';
import { Context } from 'koa';

const router = new Router({
    prefix: '/config'
});

router.get('/', async (ctx: Context, next)=> {
    return next().then(()=> {
        ctx.body = [];
    });
});

export {
    router
};
