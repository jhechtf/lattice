// import the router class
import * as Router from 'koa-router';
// Start subroutes import here. Make sure to use an alias
import {router as movieRouter} from './movies';
import {router as configRouter} from './config';
import {router as searchRouter} from './search';
import { Context } from 'koa';

// create a routes object
const routes = new Router({
    prefix: '/api'
});

/**
 * This next part is suboptimal, personally:
 * See, Node's implementation of Javascript means that require() statements are synchronous. This means that you could use the file system module
 * to read through directories, and use the require function to include an entire directory that you can work with, assuming they follow a pattern.
 * This was particularly useful in cases where you were using a Rails(or other MVC framework)-like approach to having certain directories signify
 * certain responsibilities.
 *
 * I would have _liked_ to make the routes automatic in their population, but unfortunately my insistence on using TypeScript for the backend means I
 * lose some of the latent abilities of Node. Perhaps someone who is more knowledgable with TS can point me into a way to solve this, but as far as I have
 * looked, it is simply not possible.
 */

// Add in routes here
routes.use(movieRouter .routes());
routes.use(configRouter.routes());
routes.use(searchRouter.routes());
// export the routes to be consumed by the server.ts file.
export {
    routes
};
