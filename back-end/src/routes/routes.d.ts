export declare namespace Routes {
    export interface ParamObject {
        [key: string]: any;
    }
}