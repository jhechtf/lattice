# Routes

## Basic syntax

For defined routes you must do 2 "steps". The first is that you must make an applicable file. In said file you must import the koa-router. 

You must then create a new instance of the router, and if you are planning on prefixing the routes, you must make sure to set the `prefix` key in the options.

For example

```ts
// routes/users.ts
import * as Router from 'koa-router';

const routes = new Router({
    prefix: '/movies'
});

router.get('/', (ctx)=> {
    ctx.body = 'movies!';
});

router.get('/:id', (ctx)=> {
    ctx.body = ctx.params.id;
});

export {
    router
};

```

Realistically, you could name the constant whatever you want, but the export must be named 'router'.

Now, in `routes/index.ts` you must remember to include the `users.ts`, and add it into the router, like so.

```ts
// routes.index.ts

import {router as userRouter} from './users';

// later on

routes.use(userRouter /*, ...any other routers you include*/)

export {
    routes
}
```