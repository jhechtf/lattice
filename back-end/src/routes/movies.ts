import * as Router from 'koa-router';
import { Context } from 'koa';
import {Routes} from './routes';

const router = new Router({
    prefix: '/movie'
});

// get all movies
router.get('/', async (ctx: Context, next)=> {
    const {data} = await ctx.$tmdb.get('movie/popular' , {
        params: {
            page: ctx.query.page || 1
        }
    });
    ctx.body = data;
    return next();
});
// get a specific movie.
router.get('/:id', async (ctx: Context, next)=> {
    // Make sure that our ID is numeric.
    if(/^\d+$/.test(ctx.params.id)) {
        // If we run into an error this is the only way to move safely forward
        try {
            const params: Routes.ParamObject = {};
            if(ctx.query.append) {
                params.append_to_response = ctx.query.append;
            }
            const {data} = await ctx.$tmdb.get('movie/'+ctx.params.id, {params});
            ctx.body = data;
        } catch(e) {
            console.log('Error!', e);
        }
    }
    return next();
});
// Discover new movies
router.get('/discover', async (ctx: Context, next)=> {
    const {data} = await ctx.$tmdb.get('discover/movie');
    ctx.body = data;
    return next();
});

export {
    router
};
