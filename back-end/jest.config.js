module.exports = {
    "moduleFileExtensions": [
        "ts",
        "js"
      ],
      "transform": {
        "\\.ts$": "ts-jest"
      },
      "testRegex": "/tests/.*\\.(ts|tsx|js)$"
};