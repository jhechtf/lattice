import * as request from 'supertest';
import { server } from '../src/server';
// import { request } from 'https';
beforeAll(async () => {
    console.log('jest starting');
});
afterAll(() => {
   server.close();
   console.log('Server has been closed');
});

describe('Route Testing for /api/search', ()=> {
    test('Get Base Route, no params', async () => {
        const response = await request(server).get('/api/search');
        expect(response.status).toBe(200);
        expect(response.text).toContain('[]');
    });
    test('Search for "Harry Potter"', async () => {
        const response = await request(server).get('/api/search?query=Harry+Potter');
        expect(response.status).toBe(200);

    });
});
