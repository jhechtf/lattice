// the functions describe, it, and expect are provided by jest when the tests are ran.

describe('Something', () => {
    it('Does some quantifiable action', ()=> {
        const something = 'someValue';
        expect(something).toBe('someValue');
    });
});
