FROM node:11 as base
RUN mkdir -p /var/app
WORKDIR /var/app
ADD . .
RUN yarn && yarn build

FROM node:11 as production
COPY --from=base /var/app/dist /var/app/dist
WORKDIR /var/app
ADD package.json ./
ADD yarn.lock ./
ADD .env ./
RUN mkdir -p /var/app/cache && chmod a+rw /var/app/cache && yarn --production
ENTRYPOINT ["yarn", "start"]
